# Change Log

## 1.0.0

Update Documentation Formatting and Project Structure.

### Added

- replace .gitlab-ci.yml with project-info.yaml
- remove repo-icon.png
- add LICENSE.txt

### Changed

- update build-image variables
- update README and CHANGELOG formats

### Fixed

## 0.8.0

Add Bash.

### Added

- add bash

### Changed

### Fixed

## 0.7.0

Remove Kaniko and Add QOL Packages.

### Added

- remove kaniko
- add zip
- add jq
- add yq

### Changed

- update working directory to /root

### Fixed

## 0.6.0

Add Docker Credentials ECR Login.

### Added

- add docker-credential-ecr-login for kaniko

### Changed

### Fixed

## 0.5.0

Add Git and Kaniko and Update Alpine.

### Added

- remove npm
- add git
- add kaniko

### Changed

- update alpine to 3.15
- update repo-icon.png
- update pipeline to use base-pipeline/pipelines/build-pipeline

### Fixed

## 0.4.0

Add NPM

### Added

- add npm

### Changed

### Fixed

## 0.3.0

Alpine Migration and Update AWS CLI.

### Added

- remove python

### Changed

- update base image to Alpine
- update to AWS CLI v2

### Fixed

## 0.2.0

Add GitLab Release CLI and Repo Icon

### Added

- add gitlab release-cli
- add repo-icon.png

### Changed

### Fixed

## 0.1.0

Initial project setup.

### Added

- add Dockerfile that buses Debian distro
- add python3
- add aws-cli
- add curl

### Changed

### Fixed
